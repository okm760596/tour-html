var gulp = require('gulp');
var scss = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');

const fileinclude = require('gulp-file-include');

 var concat = require('gulp-concat');
 var uglify = require('gulp-uglify');
 var rename = require('gulp-rename');
 var del = require('del');
 var changed = require('gulp-changed');
 var fileSync = require('gulp-file-sync');

// ❗ 하위뎁스 컴파일 잘안됨. file include 쪽 문제잇음.


var PATH = {
  ROOT: './workspace',
  HTML: './workspace/html',
  INC: './workspace/html/include'
},
DEST_PATH = {
  HTML: './dist/',
  INC: './dist/include'
}

const fileIncludeOpt ={
  prefix: '@@',
  basepath: '@file',
  context:{
    'webRoot' : ".",
    'htmlRoot' : '.',
    'imageRoot' : './images',
    'cssRoot': './css',
  }
}

const browsSyncOpt = {
  stream: true
}

gulp.task('clear', () => {
  return new Promise( resolve => {
    del.sync(['./dist/']);
    resolve();
  });
});


gulp.task('nodemon:start', () => {
  return new Promise( resolve => {
    nodemon({
      script: 'app.js',
      watch: 'app'
    });
  
    resolve();
  });
});


gulp.task('html', () => {
  return new Promise( resolve => {
    // try{
    gulp.src([
    PATH.HTML + '/**/*.*',
    '!'+PATH.HTML + '/include/*.html',
    '!'+PATH.HTML + '/popupinc/*.*',
    // PATH.HTML + '/plan_manage/*.html'
    ])
    // .pipe(changed(DEST_PATH.HTML))
    .pipe( fileinclude(fileIncludeOpt) )
    .pipe(gulp.dest(DEST_PATH.HTML))
    .pipe( browserSync.reload(browsSyncOpt) );
    resolve();
  });
});



gulp.task('watch', () => {
  return new Promise( resolve => {
    gulp.watch(PATH.HTML + '/**/*.*', gulp.series(['html']));
    resolve();
  });
});

gulp.task('borwserSync', () => {
  return new Promise( resolve => {
    browserSync.init({
      server: {
        baseDir: './'
      }
    }, {
      proxy: 'http://127.0.0.1:8001',
      port: 8002,
      open: false,
      notify: false,
      ghostMode: false,
    }
    );

    resolve();
  });
});

gulp.task('default', gulp.series([
  'nodemon:start',
  //'imagesSync',
  //'librarySync',
  'html',
  // 'copyfile',
  // 'fileinclude',
  //'script:copy',
  //'script:bulid',
  //'scss:compile',
  //'scssguide:compile',
  // 'guide',
  // 'guide:scss',
  'borwserSync', 
  'watch'
]));